FROM node:16-alpine

WORKDIR /app

COPY package*.json /app/

RUN yarn install

COPY . /app/

RUN yarn build

EXPOSE 4500

CMD [ "yarn", "start"]