import AverageDeterminator from '../src/services/AverageDeterminator'


describe('Baseball Average', () => {
	describe('Jugadores con un Average mayor al del Equipo', () => {
		const collectiveAverage = 0.627
		const determinator = AverageDeterminator.initWith(collectiveAverage)

		test('Juan tiene un average mayor al del equipo', () => {
			const juanPlayer = {
				name: "Juan",
				hits: 300,
				ab: 440,
				avg: 0.682
			}

			expect(determinator.hasAverageGreatherThatCollectiveAverage(juanPlayer.avg)).toBe(true);
		});

		test('Pedro tiene un average mayor al del equipo', () => {
			const juanPlayer = {
				name: "Pedro",
				hits: 220,
				ab: 300,
				avg: 0.733
			}

			expect(determinator.hasAverageGreatherThatCollectiveAverage(juanPlayer.avg)).toBe(true);
		});
	});

	describe('Jugadores con un Average menor al del Equipo', () => {
		const collectiveAverage = 0.668639053254438
		const determinator = AverageDeterminator.initWith(collectiveAverage)

		test('Daniel tiene un average mayor al del equipo', () => {
			const danielPlayer = {
				name: "Daniel",
				hits: 200,
				ab: 450,
				avg: 0.444
			}

			expect(determinator.hasAverageGreatherThatCollectiveAverage(danielPlayer.avg)).toBe(false);
		});

		test('Samuel tiene un average mayor al del equipo', () => {
			const samuelPlayer = {
				name: "Samnuel",
				hits: 210,
				ab: 445,
				avg: 0.472
			}

			expect(determinator.hasAverageGreatherThatCollectiveAverage(samuelPlayer.avg)).toBe(false);
		});
	});

	describe('Cuando el jugador tiene el mismo average que el del equipo', () => {
		const AVERAGE = 0.56
		const determinator = AverageDeterminator.initWith(AVERAGE)
		it("debería ser verdadero", () => {
			const player = {
				avg: AVERAGE
			}
			expect(determinator.hasSameAverage(player.avg)).toBe(true)
		})
	})

	describe('Cuando no tienen el mismo average', () => {
		const AVERAGE = 0.56
		const determinator = AverageDeterminator.initWith(AVERAGE)
		it("debería ser falso", () => {
			const player = {
				avg: 0.289
			}
			expect(determinator.hasSameAverage(player.avg)).toBe(false)
		})
	})
});