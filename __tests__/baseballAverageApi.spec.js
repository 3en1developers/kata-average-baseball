import request from 'supertest'

import app from '../src/app'


describe('Baseball Average API', () => {
	const server = request(app)
	describe('GET /', () => {
		it('debería mostrar el mensaje de bienvenida', async (done) => {
			const response = await server.get("/")

			expect(response.body).toEqual({ message: "Bienvenidos a Baseball Average" });
			expect(response.status).toEqual(200);
			done()
		});
	});

	describe('POST /api/v1/greater-average/', () => {
		const ENDPOINT_GREATER_AVERAGE = "/api/v1/greater-average/"

		describe('cuando el average del equipo es mayor al del jugador', () => {
			it('debería devolver el average del equipo', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({
						playerAverage: 0.567,
						teamAverage: 0.670
					})

				expect(response.status).toBe(200)
				expect(response.body).toEqual({ teamAverage: 0.670 })
				done()
			});
		});

		describe('cuando el average del jugador es mayor al del equipo', () => {
			it('debería devolver el average del jugador', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({
						playerAverage: 0.767,
						teamAverage: 0.670
					})

				expect(response.status).toBe(200)
				expect(response.body).toEqual({ playerAverage: 0.767 })
				done()
			});
		});

		describe('cuando el equipo y el jugador tiene el mismo average', () => {
			it('debería decir que tienen el mismo average', async (done) => {
				const SAME_AVERAGE = 0.897
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({
						playerAverage: SAME_AVERAGE,
						teamAverage: SAME_AVERAGE
					})

				expect(response.status).toBe(200)
				expect(response.body).toEqual({ average: "Tienen el mismo average!" })
				done()
			});
		});

		describe('cuando no pasen el body correcto en la petición', () => {
			it('da error cuando no pasan el teamAverage', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({ playerAverage: 0.93 })

				expect(response.status).toEqual(400);
				expect(response.body).toEqual({ message: "El teamAverage es requerido!" })
				done()
			})
			it('da error cuando no pasan el playerAverage', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({ teamAverage: 0.93 })

				expect(response.status).toEqual(400);
				expect(response.body).toEqual({ message: "El playerAverage es requerido!" })
				done()
			})
			it('da error cuando no pasan el playerAverage ni el teamAverage', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({})

				expect(response.status).toEqual(400);
				expect(response.body).toEqual({ message: "El playerAverage y el teamAverage son requeridos!" })
				done()
			})

			it('da error cuando el playerAverage es menor que cero', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({ playerAverage: -12, teamAverage: 12 })

				expect(response.status).toEqual(400);
				expect(response.body.message).toEqual("El playerAverage no puede ser menor que cero!")
				done()
			})

			it('da error cuando el teamAverage es menor que cero', async (done) => {
				const response = await server.post(ENDPOINT_GREATER_AVERAGE)
					.send({ playerAverage: 12, teamAverage: -1 })

				expect(response.status).toEqual(400);
				expect(response.body.message).toEqual("El teamAverage no puede ser menor que cero!")
				done()
			})
		});
	});
});