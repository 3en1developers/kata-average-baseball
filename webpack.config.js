const path = require("path")

require('dotenv').config()

const NodemonWebpackPlugin = require("nodemon-webpack-plugin")
const WebpackNodeExternals = require("webpack-node-externals")

module.exports = {
    mode: process.env.WEBPACK_MODE || "development",
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "app.bundle.js",
    },
    externals: [WebpackNodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }
        ]
    },
    plugins: [
        new NodemonWebpackPlugin()
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        port: process.env.PORT || 4500,
        compress: true
    },
    resolve: {
        extensions: ['.js']
    }
};