# Kata: Average de Bateo

![Sticker](./sticker.png)  


Un coach de bateo quiere determinar cuales son los jugadores que tiene
un average mayor al promedio de bateo del equipo.


## Herramientas utilizadas :hammer: :construction:

- NodeJs (ES6).
- Express.
- Express-validator.
- BabelJs.
- Webpack.
- Jest.
- yarn.


## ¿Cómo probar el proyecto? :syringe:

- Clona el reposiotorio.
- Accede a la carpeta.
- Haz un ```yarn install``` o ``` npm install```.
- Ejecuta la app con ``` yarn run dev``` o ```npm run dev```.
- Listo!


## End Points :electric_plug:

 - POST ***/api/v1/greater-average/*** 

 ```
{
	playerAverage: float,
    teamAverage: float
}
 ```