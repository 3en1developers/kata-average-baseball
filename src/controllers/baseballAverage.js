import { validationResult } from 'express-validator'

import AverageDeterminator from '../services/AverageDeterminator'


export const greaterAverage = (req, resp) => {
	const { playerAverage, teamAverage } = req.body
	const determinator = AverageDeterminator.initWith(teamAverage)

	

	if (determinator.hasSameAverage(playerAverage)) {
		resp.status(200).json({ average: "Tienen el mismo average!" })
	} else if (determinator.hasAverageGreatherThatCollectiveAverage(playerAverage)) {
		resp.status(200).json({ playerAverage })
	} else {
		resp.status(200).json({ teamAverage })
	}
}
