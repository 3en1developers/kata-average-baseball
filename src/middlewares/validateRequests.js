import { validationResult } from 'express-validator'


export const checkGreaterAverageRequest = (req, resp, next) => {
	const { playerAverage, teamAverage } = req.body
	if (playerAverage === undefined && teamAverage === undefined) {
		resp.status(400).json({message: "El playerAverage y el teamAverage son requeridos!"})
	}

	if (teamAverage == undefined) {
		resp.status(400).json({message: "El teamAverage es requerido!"})
	}

	if (playerAverage == undefined) {
		resp.status(400).json({message: "El playerAverage es requerido!"})
	}
	next()
}

export const checkError = (req, resp, next) => {
	const errors = validationResult(req)
	if (!errors.isEmpty()) {
		resp.status(400).json({message: errors.array()[0].msg})
	}
	next()
}