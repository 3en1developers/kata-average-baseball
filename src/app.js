import cors from 'cors'
import dotenv from 'dotenv'
import express from 'express'
import morgan from 'morgan'

import router from './routes/app'

dotenv.config()

const MORGAN_MODE = process.env.MORGAN_MODE || "short"

const app = express()
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(morgan(MORGAN_MODE))
app.use(cors())
app.use('/api/v1/', router)


app.get('/', (req, resp) => {
	resp.json({message: "Bienvenidos a Baseball Average"})
})


export default app