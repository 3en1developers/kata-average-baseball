class AverageDeterminator {
   static initWith(collectiveAverage) {
		return new AverageDeterminator(collectiveAverage)
   }

   constructor(collectiveAverage) {
		this.collectiveAverage = collectiveAverage
   }

   hasAverageGreatherThatCollectiveAverage(playerAverage) {
		return playerAverage > this.collectiveAverage
   }

	 hasSameAverage(playerAverage) {
		 return playerAverage === this.collectiveAverage
	 }
}

export default AverageDeterminator