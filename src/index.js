import dotenv from 'dotenv' 

import app from './app'

dotenv.config()

const PORT = process.env.PORT || 4500

app.listen(PORT, () => {
	console.log(`Ejecutando la app en el puerto ${PORT}`)
})