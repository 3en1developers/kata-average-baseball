import { Router } from 'express'

import { check } from 'express-validator'

import { greaterAverage } from '../controllers/baseballAverage'
import { checkError, checkGreaterAverageRequest } from '../middlewares/validateRequests'


const router = Router()

router.post('/greater-average',
	[
		check('playerAverage')
			.isFloat({ min: 0 })
			.withMessage('El playerAverage no puede ser menor que cero!'),
		check('teamAverage')
			.isFloat({ min: 0 })
			.withMessage('El teamAverage no puede ser menor que cero!')
	],
	checkGreaterAverageRequest,
	checkError,
	greaterAverage
)

export default router